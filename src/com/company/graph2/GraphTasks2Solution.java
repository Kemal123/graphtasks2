package com.company.graph2;

import java.util.HashMap;

public class GraphTasks2Solution implements GraphTasks2 {

    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        Dijkstra dijkstra = new Dijkstra();
        return dijkstra.goDijkstra(adjacencyMatrix, startIndex);
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        Prime prime = new Prime();
        return prime.algorithmPrime(adjacencyMatrix);
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        Kruskal kruskal = new Kruskal();
        return kruskal.kruskalAlgorithm(adjacencyMatrix);
    }
}
