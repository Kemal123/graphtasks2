package com.company.graph2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

public class Prime {

    public static void main(String[] args) {
        Prime prime = new Prime();
        System.out.println(prime.algorithmPrime(new int[][]{
                {0, 5, 0, 0, 3, 0},
                {5, 0, 3, 7, 0, 0},
                {0, 3, 0, 0, 0, 11},
                {0, 7, 0, 0, 1, 42},
                {3, 0, 0, 1, 0, 0},
                {0, 0, 11, 42, 0, 0}}));

        System.out.println(prime.algorithmPrime(new int[][]
                {{0, 2, 0, 0, 0},
                        {2, 0, 4, 0, 0},
                        {0, 4, 0, 2, 0},
                        {0, 0, 2, 0, 2},
                        {0, 0, 0, 2, 0}}));

        System.out.println(prime.algorithmPrime(new int[][]{{0, 1, 0, 1},
                {1, 0, 1, 0},
                {0, 1, 0, 1},
                {1, 0, 1, 0}}));
    }


    int algorithmPrime(int[][] adjacencyMatrix) {
        ArrayList<Arc> arcsSorted = new ArrayList<>();
        int counterForArray = 0;
        for (int i = 0; i < adjacencyMatrix.length; i++) {

            for (int j = 0; j < counterForArray; j++) {
                if (adjacencyMatrix[i][j] != 0) {
                    arcsSorted.add(new Arc(i, j, adjacencyMatrix[i][j]));
                }
            }
            counterForArray += 1;
        }
        Collections.sort(arcsSorted);
        Stack<Integer> usedVertexes = new Stack<>();
        int result = 0;
        usedVertexes.add(arcsSorted.get(0).getV1());
        usedVertexes.add(arcsSorted.get(0).getV2());
        result += arcsSorted.get(0).getWeight();
        arcsSorted.remove(0);

        while (!arcsSorted.isEmpty()) {
            for (int i = 0; i < arcsSorted.size(); i++) {
                if (usedVertexes.contains(arcsSorted.get(i).getV1()) ^ usedVertexes.contains(arcsSorted.get(i).getV2())) {
                    usedVertexes.add(arcsSorted.get(i).getV1());
                    usedVertexes.add(arcsSorted.get(i).getV2());
                    result += arcsSorted.get(i).getWeight();
                    arcsSorted.remove(i);
                    break;
                } else {
                    if (usedVertexes.contains(arcsSorted.get(i).getV1()) && usedVertexes.contains(arcsSorted.get(i).getV2())) {
                        arcsSorted.remove(i);
                        break;
                    }
                }
            }
        }

        return result;
    }
}




