package com.company.graph2;

import java.util.ArrayDeque;
import java.util.ArrayList;

public class Graph {

    public static void main(String[] args) {

        ArrayList<String> strings = new ArrayList<String>();
        Graph graph = new Graph();
        System.out.println(graph.breadthFirst(new boolean[][]{{false, true, true, false, false},
                {true, false, true, false, false},
                {true, true, false, true, true},
                {false, false, true, false, true},
                {false, false, true, true, false}}, 3));
    }


    public String breadthFirst(boolean[][] adjacencyMatrix, int startIndex) {

        ArrayDeque<Integer> vertexes = new ArrayDeque<Integer>();
        ArrayList<Integer> result = new ArrayList<Integer>();
        vertexes.addFirst(startIndex);
        result.add(startIndex);
        while (!vertexes.isEmpty()) {
            for (int i = 0; i < adjacencyMatrix[startIndex].length; i++) {
                if (adjacencyMatrix[startIndex][i]) {
                    if (check(result, i)) {
                        vertexes.addLast(i);
                        result.add(i);
                    }
                }

            }
            vertexes.pollFirst();
            try {
                startIndex = vertexes.peekFirst();
            } catch (NullPointerException e) {
                String size = result.toString();
                return (result.toString().substring(1, size.length() - 1));
            }
        }
        return null;
    }

    boolean check(ArrayList<Integer> result, Integer check) {

        for (int i = 0; i < result.size(); i++) {
            if (result.get(i).equals(check)) {
                return false;
            }
        }
        return true;
    }

}
