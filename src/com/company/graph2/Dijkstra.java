package com.company.graph2;

import java.util.ArrayList;
import java.util.HashMap;

public class Dijkstra {
    public static void main(String[] args) {
        Dijkstra dijkstra = new Dijkstra();
        System.out.println(dijkstra.goDijkstra(new int[][]{
                {0, 5, 0, 0, 3, 0},
                {5, 0, 3, 7, 0, 0},
                {0, 3, 0, 0, 0, 11},
                {0, 7, 0, 0, 1, 42},
                {3, 0, 0, 1, 0, 0},
                {0, 0, 11, 42, 0, 0}}, 0));
    }

    HashMap<Integer, Integer> goDijkstra(int[][] adjacencyMatrix, int startIndex) {

        HashMap<Integer, Integer> graph = new HashMap<>();
        ArrayList<Integer> checkVertex = new ArrayList<>();  //просмотренные вершины

        graph.put(startIndex, 0);
        checkVertex.add(0);
        int wight = 0;
        int INF = Integer.MAX_VALUE;
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            if (i != startIndex) {
                graph.put(i, INF);
            }
        }

        int flag = startIndex;

        for (int i = 0; i < adjacencyMatrix.length; i++) {


            for (int j = 0; j < adjacencyMatrix[flag].length; j++) {
                if (adjacencyMatrix[flag][j] != 0) {
                    if (!checkVertex.contains(j)) {
                        wight = graph.get(flag) + adjacencyMatrix[flag][j];
                        if (wight < graph.get(j)) {
                            graph.put(j, wight);
                        }
                    }
                }
            }
            flag = checkKey(graph, adjacencyMatrix, flag, checkVertex);
            checkVertex.add(flag);


        }

        return graph;

    }

    int checkKey(HashMap<Integer, Integer> graph, int[][] adjacencyMatrix, int flag, ArrayList<Integer> checkVertex) {
        int INF = Integer.MAX_VALUE;
        int key = 0;
        for (int i = 0; i < adjacencyMatrix[flag].length; i++) {
            if (adjacencyMatrix[flag][i] != 0) {
                if (INF > adjacencyMatrix[flag][i] & !checkVertex.contains(i)) {
                    INF = adjacencyMatrix[flag][i];
                    key = i;
                }
            }
        }
        return key;
    }

}
