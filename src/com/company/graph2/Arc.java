package com.company.graph2;

public class Arc implements Comparable<Arc> {
    Integer v1;
    Integer v2;
    Integer weight;

    public Arc(int v1, int v2, int weight) {
        this.v1 = v1;
        this.v2 = v2;
        this.weight = weight;
    }

    public Integer getV1() {
        return v1;
    }


    public Integer getV2() {
        return v2;
    }


    public Integer getWeight() {
        return weight;
    }


    @Override
    public int compareTo(Arc o) {
        return this.weight.compareTo(o.getWeight());
    }

    @Override
    public String toString() {
        return "Arc{" +
                "v1=" + v1 +
                ", v2=" + v2 +
                ", weight=" + weight +
                '}';
    }
}
