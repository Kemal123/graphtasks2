package com.company.graph2;

import com.company.graph2.Arc;
import com.company.graph2.Graph;

import java.util.*;

public class Kruskal {
    public static void main(String[] args) {


        Kruskal kruskal = new Kruskal();
        System.out.println(kruskal.kruskalAlgorithm(new int[][]{
                {0, 5, 0, 0, 3, 0},
                {5, 0, 3, 7, 0, 0},
                {0, 3, 0, 0, 0, 11},
                {0, 7, 0, 0, 1, 42},
                {3, 0, 0, 1, 0, 0},
                {0, 0, 11, 42, 0, 0}}));

        System.out.println(kruskal.kruskalAlgorithm(new int[][]
                {{0, 2, 0, 0, 0},
                        {2, 0, 4, 0, 0},
                        {0, 4, 0, 2, 0},
                        {0, 0, 2, 0, 2},
                        {0, 0, 0, 2, 0}}));

        System.out.println(kruskal.kruskalAlgorithm(new int[][]{{0, 1, 0, 1},
                {1, 0, 1, 0},
                {0, 1, 0, 1},
                {1, 0, 1, 0}}));
    }


    int kruskalAlgorithm(int[][] adjacencyMatrix) {
        ArrayList<Arc> arcsSorted = new ArrayList<>();
        int counterForArray = 0;
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = 0; j < counterForArray; j++) {
                if (adjacencyMatrix[i][j] != 0) {
                    arcsSorted.add(new Arc(i, j, adjacencyMatrix[i][j]));
                }
            }
            counterForArray += 1;
        }

        Collections.sort(arcsSorted);

        int result = 0;

        boolean[][] graph = new boolean[adjacencyMatrix.length][adjacencyMatrix.length];
        Graph graph1 = new Graph();
        while (!arcsSorted.isEmpty()) {
            Arc arc = arcsSorted.get(0);
            if (!graph1.breadthFirst(graph, arc.getV1()).contains(arc.getV2().toString())) {
                graph[arc.getV1()][arc.getV2()] = true;
                graph[arc.getV2()][arc.getV1()] = true;
                result += arc.getWeight();
            }
            arcsSorted.remove(0);
        }
        return result;
    }
}

